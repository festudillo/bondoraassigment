﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BondoraAssigment.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BondoraAssigment.Models;

namespace BondoraAssigment.Controllers.Tests
{
    [TestClass()]
    public class EquipmentRentalsControllerTests
    {
        [TestMethod()]
        public void GetLoyaltyPointsCreationTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            Equipment newEquipment = new Equipment()
            {
                Id = 1,
                Name = "Equipment 1",
                Type = EquipmentType.Regular
            };
            EquipmentRental newEquipmentRental = new EquipmentRental()
            {
                Days = 5,
                EquipmentToRent = newEquipment,
                EquipmentToRentId = newEquipment.Id,
                Fee = 0,
                Id = 1
            };
            // Act
            var result = controller.GetLoyaltyPoints(newEquipmentRental, null, newEquipment, null);


            // Assert
            Assert.AreEqual(result, 1);
        }

        [TestMethod()]
        public void GetLoyaltyPointsEditUpTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            Equipment newEquipment = new Equipment()
            {
                Id = 1,
                Name = "Equipment 1",
                Type = EquipmentType.Regular
            };
            EquipmentRental newEquipmentRental = new EquipmentRental()
            {
                Days = 5,
                EquipmentToRent = newEquipment,
                EquipmentToRentId = newEquipment.Id,
                Fee = 0,
                Id = 1
            };
            Equipment oldEquipment = new Equipment()
            {
                Id = 2,
                Name = "Equipment 2",
                Type = EquipmentType.Heavy
            };
            EquipmentRental oldEquipmentRental = new EquipmentRental()
            {
                Days = 5,
                EquipmentToRent = oldEquipment,
                EquipmentToRentId = oldEquipment.Id,
                Fee = 0,
                Id = 2
            };
            // Act
            var result = controller.GetLoyaltyPoints(newEquipmentRental, oldEquipmentRental, newEquipment, oldEquipment);


            // Assert
            Assert.AreEqual(result, -1);
        }


        [TestMethod()]
        public void GetLoyaltyPointsEditDownTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            Equipment oldEquipment = new Equipment()
            {
                Id = 1,
                Name = "Equipment 1",
                Type = EquipmentType.Regular
            };
            EquipmentRental oldEquipmentRental = new EquipmentRental()
            {
                Days = 5,
                EquipmentToRent = oldEquipment,
                EquipmentToRentId = oldEquipment.Id,
                Fee = 0,
                Id = 2
            };
            Equipment newEquipment = new Equipment()
            {
                Id = 2,
                Name = "Equipment 2",
                Type = EquipmentType.Heavy
            };
            EquipmentRental newEquipmentRental = new EquipmentRental()
            {
                Days = 5,
                EquipmentToRent = newEquipment,
                EquipmentToRentId = newEquipment.Id,
                Fee = 0,
                Id = 1
            };
            // Act
            var result = controller.GetLoyaltyPoints(newEquipmentRental, oldEquipmentRental, newEquipment, oldEquipment);


            // Assert
            Assert.AreEqual(result, 1);
        }

        [TestMethod()]
        public void CalculateFeeRegularTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            // Act
            var result = controller.CalculateFee(5, EquipmentType.Regular);


            // Assert
            Assert.AreEqual(result, 340);
        }

        [TestMethod()]
        public void CalculateFeeHeavyTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            // Act
            var result = controller.CalculateFee(5, EquipmentType.Heavy);


            // Assert
            Assert.AreEqual(result, 400);
        }

        [TestMethod()]
        public void CalculateFeeSpecializedTest()
        {
            EquipmentRentalsController controller = new EquipmentRentalsController();
            // Act
            var result = controller.CalculateFee(5, EquipmentType.Specialized);


            // Assert
            Assert.AreEqual(result, 360);
        }
    }
}