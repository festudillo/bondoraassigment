﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BondoraAssigment.Models
{
    public enum EquipmentType : byte
    {
        [Display(Name = "Heavy")]
        Heavy,

        [Display(Name = "Regular")]
        Regular,

        [Display(Name = "Specialized")]
        Specialized
    }
}