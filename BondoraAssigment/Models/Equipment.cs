﻿using System.ComponentModel.DataAnnotations;

namespace BondoraAssigment.Models
{
    public class Equipment
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Equipment Type is required")]
        public EquipmentType Type { get; set; }
    }
}