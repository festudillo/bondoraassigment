﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BondoraAssigment.Models
{
    public class EquipmentRental
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Equipment is required")]
        [DisplayName("Equipment To Rent")]
        public int EquipmentToRentId { get; set; }

        public int Fee { get; set; }

        [Required(ErrorMessage = "Days is required")]
        public int Days { get; set; }

        public virtual Equipment EquipmentToRent { get; set; }
    }
}