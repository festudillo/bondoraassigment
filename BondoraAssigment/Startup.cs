﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BondoraAssigment.Startup))]
namespace BondoraAssigment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
