﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using BondoraAssigment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace BondoraAssigment.Controllers
{
    [Authorize]
    public class EquipmentRentalsController : Controller
    {
        private ApplicationUserManager _userManager;
        private readonly BondoraAssignmentContext _db = new BondoraAssignmentContext();
        private static MemoryCache _cache = MemoryCache.Default;

        // GET: EquipmentRentals
        public ActionResult Index()
        {
            List<EquipmentRental> equipmentRentalList;
            if (!_cache.Contains("EquipmentRentalList"))
            {
                equipmentRentalList = _db.EquipmentRentals.Include(a => a.EquipmentToRent).ToList();
                _cache.Add("EquipmentRentalList", equipmentRentalList, DateTimeOffset.MaxValue);
            }
            else
            {
                equipmentRentalList = _cache.Get("EquipmentRentalList") as List<EquipmentRental>;
            }
            return View(equipmentRentalList);
        }


        public ActionResult GetInvoice()
        {
            var name = Server.MapPath("~/invoices/invoice-" + DateTime.Now.Ticks + ".txt");
            var info = new FileInfo(name);
            var equipmentRentals = _db.EquipmentRentals.Include(a => a.EquipmentToRent).ToList();
            using (StreamWriter writer = info.CreateText())
            {
                writer.WriteLine("INVOICE");
                var totalPrice = 0;
                var totalLoyaltyPoints = 0;
                foreach (var equipmentRental in equipmentRentals)
                {
                    writer.WriteLine("Name: " + equipmentRental.EquipmentToRent.Name + ", Price: " + equipmentRental.Fee);
                    totalPrice += equipmentRental.Fee;
                    totalLoyaltyPoints += equipmentRental.EquipmentToRent.Type == EquipmentType.Heavy ? 2 : 1;
                }
                writer.WriteLine("Total Price: " + totalPrice + ", Loyalty Points: " + totalLoyaltyPoints);
            }
            return File(info.OpenRead(), "text/plain", Server.UrlEncode(name));
        }

        // GET: EquipmentRentals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EquipmentRental equipmentRental;
            if (!_cache.Contains("EquipmentRental" + id))
            {
                equipmentRental = _db.EquipmentRentals.Find(id);
                _cache.Add("EquipmentRental" + id, equipmentRental, DateTimeOffset.MaxValue);
            }
            else
            {
                equipmentRental = _cache.Get("EquipmentRental" + id) as EquipmentRental;
            }
            if (equipmentRental == null)
            {
                return HttpNotFound();
            }
            return View(equipmentRental);
        }

        // GET: EquipmentRentals/Create
        public ActionResult Create()
        {
            ViewBag.EquipmentToRentId = new SelectList(_db.Equipments, "Id", "Name");
            return View();
        }

        // POST: EquipmentRentals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Fee,Days,EquipmentToRentId")] EquipmentRental equipmentRental)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var u = UserManager.FindById(userId);
                var equipment = _db.Equipments.Find(equipmentRental.EquipmentToRentId);
                if (equipment != null)
                {
                    u.LoyaltyPoints += GetLoyaltyPoints(equipmentRental, null, equipment, null);
                    equipmentRental.Fee = CalculateFee(equipmentRental.Days, equipment.Type);
                }
                _db.EquipmentRentals.Add(equipmentRental);
                _db.SaveChanges();
                UserManager.Update(u);
                if (_cache.Contains("EquipmentRentalList"))
                {
                    _cache.Remove("EquipmentRentalList");
                }
                return RedirectToAction("Index");
            }
            ViewBag.EquipmentToRentId = new SelectList(_db.Equipments, "Id", "Name");

            return View(equipmentRental);
        }

        // GET: EquipmentRentals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EquipmentRental equipmentRental = _db.EquipmentRentals.Find(id);
            ViewBag.EquipmentToRentId = new SelectList(_db.Equipments, "Id", "Name", equipmentRental.EquipmentToRentId);
            if (equipmentRental == null)
            {
                return HttpNotFound();
            }
            return View(equipmentRental);
        }

        // POST: EquipmentRentals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Fee,Days,EquipmentToRentId")] EquipmentRental equipmentRental)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var u = UserManager.FindById(userId);
                var equipment = _db.Equipments.AsNoTracking().First(p => p.Id == equipmentRental.EquipmentToRentId);
                var oldEquipmentRental = _db.EquipmentRentals.AsNoTracking().First(p => p.Id == equipmentRental.Id);
                var oldEquipment = _db.Equipments.Find(oldEquipmentRental.EquipmentToRentId);
                if (equipment != null)
                {
                    u.LoyaltyPoints += GetLoyaltyPoints(equipmentRental, oldEquipmentRental, equipment, oldEquipment);
                    equipmentRental.Fee = CalculateFee(equipmentRental.Days, equipment.Type);
                }
                _db.Entry(equipmentRental).State = EntityState.Modified;
                _db.SaveChanges();
                UserManager.Update(u);
                if (_cache.Contains("EquipmentRental" + equipmentRental.Id))
                {
                    _cache.Remove("EquipmentRental" + equipmentRental.Id);
                }
                if (_cache.Contains("EquipmentRentalList"))
                {
                    _cache.Remove("EquipmentRentalList");
                }
                return RedirectToAction("Index");
            }
            ViewBag.EquipmentToRentId = new SelectList(_db.Equipments, "Id", "Name", equipmentRental.EquipmentToRentId);
            return View(equipmentRental);
        }

        // GET: EquipmentRentals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EquipmentRental equipmentRental = _db.EquipmentRentals.Find(id);
            if (equipmentRental == null)
            {
                return HttpNotFound();
            }
            return View(equipmentRental);
        }

        // POST: EquipmentRentals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EquipmentRental equipmentRental = _db.EquipmentRentals.Find(id);
            var userId = User.Identity.GetUserId();
            var u = UserManager.FindById(userId);
            var equipment = _db.Equipments.AsNoTracking().First(p => p.Id == equipmentRental.EquipmentToRentId);
            u.LoyaltyPoints -= GetLoyaltyPoints(equipmentRental, null, equipment, null);
            _db.EquipmentRentals.Remove(equipmentRental);
            _db.SaveChanges();
            UserManager.Update(u);
            if (_cache.Contains("EquipmentRental" + equipmentRental.Id))
            {
                _cache.Remove("EquipmentRental" + equipmentRental.Id);
            }
            if (_cache.Contains("EquipmentRentalList"))
            {
                _cache.Remove("EquipmentRentalList");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public int CalculateFee(int days, EquipmentType type)
        {
            var result = 100;
            const int premiumFee = 60;
            const int regularFee = 40;
            if (type == EquipmentType.Regular && days > 2)
            {
                result += (days - 2) * regularFee + 2 * premiumFee;
            }
            else if (type == EquipmentType.Specialized && days > 3)
            {
                result += (days - 3) * regularFee + 3 * premiumFee;
            }
            else
            {
                result += days * premiumFee;
            }
            return result;
        }

        public int GetLoyaltyPoints(EquipmentRental newEquipmentRental, EquipmentRental oldEquipmentRental, Equipment newEquipment, Equipment oldEquipment)
        {
            var result = 0;
            if (oldEquipmentRental == null)
            {
                return newEquipment.Type == EquipmentType.Heavy ? 2 : 1;
            }
            if (oldEquipmentRental.EquipmentToRentId != newEquipmentRental.EquipmentToRentId)
            {
                result = oldEquipment.Type == newEquipment.Type ? 0 : (newEquipment.Type == EquipmentType.Heavy ? 1 : -1);
            }
            return result;
        }
    }
}
